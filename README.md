## Simple Pricelist AC-NH

This is a simple page for checking prices of items for *Animal Crossing - New Horizons*.
I was just sick of all the add-overflowed pages. Don't expect something special. 

The web page displays the retail prices from fish and insects items sold at Nook's, as well as 
the prices for some other items e.g. shells.

### Update History:

- 03.07.2020: included deep sea creatures (partially)
- 02.06.2020: june update
- 01.05.2020: may update
- 23.04.2020: added Gerd as a new guest ('wuh, finally bushes')

### Impressions:

**Main page**

![Screenshot non collapsed side](pictures/mainPage.png)

I (and also my friends) use this website primarily on mobile browsers, so I used collapsible containers 
instead of normal menu points to make the website more clear. Then the user can expand whatever category he wants,
but only one at a time. 

**Cover page with expanded part**

![Screenshot expanded menu](pictures/expandedPage.png)

Any constructive suggestions for improvements (especially for the code) are very welcomed. 
But as I said, it's just a small project, and I don't put too much effort in it. 
